FROM postgres:latest

ENV POSTGRES_PASSWORD postgres 
ENV POSTGRES_DB usersdb
ENV POSTGRES_USER postgres

COPY requirements.txt ./
RUN apt-get update && apt-get install -y python3 python3-pip
RUN apt-get -y install python3.7-dev

RUN pip3 install -r requirements.txt

RUN mkdir /db

COPY . .

RUN [ "python3", "src/Database.py" ]

CMD ["/bin/bash", "-l"]
EXPOSE 5432